<?php

abstract class CommentsMigration extends Migration {
  public function __construct(array $arguments) {
    parent::__construct();
    $comment_type = isset($arguments['comment type']) ? $arguments['comment type'] : NULL;
    $this->description = t('Migrate comments');
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'cid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'd6 Unique comment ID',
          'alias' => 'c',
        )
      ),
      MigrateDestinationComment::getKeySchema()
    );

    $query = $this->comment_query($arguments);

    $this->source = new MigrateSourceSQL($query);
    $this->source->setMapJoinable(FALSE);
    $this->destination = new MigrateDestinationComment($comment_type);

    $this->addSimpleMappings(array('cid', 'pid', 'nid', 'uid', 'subject', 'hostname', 'thread', 'name', 'mail', 'homepage'));
    $this->addFieldMapping('status')->defaultValue(TRUE);
    $this->addFieldMapping('language')->defaultValue('und');
    $body_arguments = MigrateTextFieldHandler::arguments(NULL, array('source_field' => 'format'), NULL);
    $this->addFieldMapping('comment_body', 'comment')->arguments($body_arguments);
    $this->addFieldMapping('created', 'timestamp');
  }

  public function comment_query($arguments = array()) {
    $type = isset($arguments['node type']) ? $arguments['node type'] : NULL;
    $query = Database::getConnection('d6')
      ->select('comments', 'c')
      ->fields('c', array('cid', 'pid', 'nid', 'uid', 'subject', 'comment', 'hostname', 'timestamp', 'status', 'format', 'thread', 'name', 'mail', 'homepage'));
    $query->join('node', 'n', 'c.nid = n.nid');
    $query->condition('n.type', $type);
    $query->orderBy('c.cid');
    return $query;
  }
  // Change this to match your site.
  public function prepareRow($current_row) {
    $formats = array(
      '1' => 'filtered_html',
      '2' => 'full_html',
      '3' => 'plain_text',
      '4' => 'markdown',
    );
    $current_row->format = isset($formats[$current_row->format]) ? $formats[$current_row->format] : 'plain_text';
  }
}

class PageCommentsMigration extends CommentsMigration {
  public function __construct() {
    parent::__construct(array('node type' => 'page', 'comment type' => 'comment_node_page'));
  }
}

class BlogCommentsMigration extends CommentsMigration {
  public function __construct() {
    parent::__construct(array('node type' => 'story', 'comment type' => 'comment_node_article'));
  }
}

class BlogCommentsMigration extends CommentsMigration {
  public function __construct() {
    parent::__construct(array('node type' => 'blog', 'comment type' => 'comment_node_blog'));
  }
}

class BookCommentsMigration extends CommentsMigration {
  public function __construct() {
    parent::__construct(array('node type' => 'book', 'comment type' => 'comment_node_book'));
  }
}
