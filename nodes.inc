<?php

abstract class NodesMigration extends Migration {
  public function __construct(array $arguments) {
    parent::__construct();
    $type = isset($arguments['type']) ? $arguments['type'] : NULL;
    $new_type = isset($arguments['new type']) ? $arguments['new type'] : $type;
    $this->description = t('Migrate nodes');
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'nid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'd6 Unique User ID',
          'alias' => 'n',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $query = $this->node_query($type);
    $this->set_highwater_field();
    $this->set_source($query);
    $this->destination = new MigrateDestinationNode($new_type);
    $this->node_field_mapping();
  }

  public function set_highwater_field() {
    $this->highwaterField = array(
      'name' => 'changed',
      'alias' => 'n',
    );
  }

  public function node_query($type) {
    $query = Database::getConnection('d6')
      ->select('node', 'n')
      ->fields('n', array('nid', 'vid', 'title', 'uid', 'status', 'created', 'changed', 'comment', 'promote', 'moderate', 'sticky'))
      ->condition('n.type', $type);
    $query->join('node_revisions', 'nr', 'nr.vid = n.vid');
    $query->fields('nr', array('body', 'teaser', 'format'));
    return $query;
  }

  public function set_source($query) {
    $this->source = new MigrateSourceSQL($query);
    $this->source->setMapJoinable(FALSE);
  }

  public function node_field_mapping() {
    $body_arguments = MigrateTextFieldHandler::arguments(array('source_field' => 'teaser'), array('source_field' => 'format'), NULL);
    // Make the mappings
    $this->addFieldMapping('nid', 'nid');
    $this->addFieldMapping('tnid', 'nid');
    $this->addFieldMapping('vid', 'vid');
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('uid', 'uid');
    $this->addFieldMapping('status', 'status');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('changed', 'changed');
    $this->addFieldMapping('comment', 'comment');
    $this->addFieldMapping('promote', 'promote');
    $this->addFieldMapping('moderate', 'moderate');
    $this->addFieldMapping('sticky', 'sticky');
    $this->addFieldMapping('body', 'body')->arguments($body_arguments);
    $this->addFieldMapping('language')->defaultValue('en');
    $this->addFieldMapping('is_new')->defaultValue(TRUE);
  }

  public function prepareRow($current_row) {
    $formats = array(
      '1' => 'filtered_html',
      '2' => 'full_html',
      '3' => 'plain_text',
      '4' => 'markdown',
    );
    $current_row->format = isset($formats[$current_row->format]) ? $formats[$current_row->format] : 'plain_text';
  }
}

class NodesPagesMigration extends NodesMigration {
  public function __construct() {
    parent::__construct(array('type' => 'page'));
  }
}

class NodesArticleMigration extends NodesMigration {
  public function __construct() {
    parent::__construct(array('type' => 'story', 'new type' => 'article'));
  }
}

class NodesBlogMigration extends NodesMigration {
  public function __construct() {
    parent::__construct(array('type' => 'blog'));
  }
}

class NodesBookMigration extends NodesMigration {
  public function __construct() {
    parent::__construct(array('type' => 'book'));
  }
}

/*
class NodesImageMigration extends NodesMigration {
  public function __construct() {
    parent::construct(array('type' => 'image'));
  }

  // Example Field mapping for an arbitrary upload migrated to a field named
  // field_upload. The prepareRow() method is used in conjuncture with this to
  // actually map data into the field.
  public function node_field_mapping() {
    parent::node_field_mapping();
    $image_file_arguments = MigrateFileFieldHandler::arguments(NULL, 'file_copy', FILE_EXISTS_REPLACE);
    $this->addFieldMapping('field_image', 'image')->arguments($image_file_arguments);
    $this->addFieldMapping('field_image_gallery', 'terms')->separator(',');
  }

  public function prepareRow($current_row) {
    parent::prepareRow($current_row);
    migrate_sandbox_file_to_file_field($current_row, 'image', 'http://www.example.com', 'd6');
    $terms = migrate_sandbox_retrieve_tags($current_row->nid);
    // Assuming our remote vocabulary id is 6...
    if (isset($terms[6])) {
      $current_row->terms = implode(',', $terms[6]);
    }
  }
}
*/

abstract class NodesBookMigration extends NodesMigration {
  public function __construct() {
    parent::__construct(array('type' => 'book'));
  }

  public function node_query($type) {
    $query = parent::node_query($type);
    $query->join('book', 'b', 'n.nid = b.nid');
    $query->fields('b', array('bid'));
    $query->join('menu_links', 'ml', 'ml.mlid = b.mlid');
    $query->fields('ml', array('mlid', 'plid'));
    return $query;
  }

  public function node_field_mapping() {
    parent::node_field_mapping();
    $this->addFieldMapping('book', 'book');
  }

  public function complete($node, $row) {
    $var = variable_get('book_mlids', array());
    $query = Database::getConnection('d6')
      ->select('menu_links', 'ml')
      ->fields('ml');
    $query->join('book', 'b', 'ml.mlid = b.mlid');
    $query->fields('b', array('bid'));
    $query->condition('b.nid', $node->nid);
    $result = $query->execute()->fetchObject();
    if ($result) {
      $node->book['bid'] = $result->bid;
      $node->book['nid'] = $node->nid;
      $node->book['plid'] = isset($var[$result->menu_name][$result->plid]) ? $var[$result->menu_name][$result->plid] : $result->plid;
      $node->book['menu_name'] = $result->menu_name;
      $node->book['weight'] = $result->weight;
    }
    _book_update_outline($node);
    node_save($node);
    $var[$result->menu_name][$result->mlid] = $node->book['mlid'];
    variable_set('book_mlids', $var);
  }
}
/*
// Example book handling classes.
class NodesBook102Migration extends NodesBookMigration {
  public function node_query($type) {
    $query = parent::node_query($type);
    $query->condition('b.bid', 102);
    $query->orderBy('FIELD(b.mlid,
      1138,
      277,
      1343,
      232,
      1139,
      1343,
      1138,
      1141,
      1342,
      1140
    )', '');
    return $query;
  }
}

class NodesBookNot102Migration extends NodesBookMigration {
  public function node_query($type) {
    $query = parent::node_query($type);
    $query->condition('b.bid', 102, '<>');
    $query->orderBy('b.bid')
      ->orderBy('ml.plid')
      ->orderBy('b.nid');
    return $query;
  }
}
*/

