<?php

abstract class TermMigration extends Migration {
  public function __construct(array $arguments) {
    $vid = isset($arguments['vid']) ? $arguments['vid'] : NULL;
    $vocabulary_name = isset($arguments['vocabulary_name']) ? $arguments['vocabulary_name'] : NULL;
    $description = isset($arguments['description']) ? $arguments['description'] : NULL;
    parent::__construct();
    $this->description = $description;
    $this->map = new MigrateSQLMap(
      $this->machineName,
      array(
        'tid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'alias' => 'td',
        )
      ),
        MigrateDestinationTerm::getKeySchema()
      );

    $query = $this->term_query($vid);
    $this->source = new MigrateSourceSQL($query);
    $this->source->setMapJoinable(FALSE);
    $this->destination = new MigrateDestinationTerm($vocabulary_name);

    // Mapped fields
    $this->term_field_mapping();
  }

  public function term_query($vid) {
    $query = Database::getConnection('d6')
      ->select('term_data', 'td')
      ->fields('td', array('tid', 'name', 'description', 'weight'))
      ->condition('vid', $vid);
    $query->join('term_hierarchy', 'th', 'th.tid = td.tid');
    $query->fields('th', array('parent'))
      // This sort assures that parents are saved before children.
      ->orderBy('parent', 'ASC');
    return $query;
  }

  public function term_field_mapping() {
    $this->addFieldMapping('tid', 'tid');
    $this->addFieldMapping('name', 'name');
    $this->addFieldMapping('description', 'description');
    $this->addFieldMapping('parent', 'parent');
    $this->addFieldMapping('weight', 'weight');
    $this->addFieldMapping('is_new')->defaultValue(TRUE);
  }

  public function prepareRow($current_row) {
    return TRUE;
  }
}

class TagsTermMigration extends TermMigration {
  public function __construct() {
    $arguments = array('vid' => '1', 'vocabulary_name' => 'tags', 'description' => t('Migrate Tags'));
    parent::__construct($arguments);
  }
}

