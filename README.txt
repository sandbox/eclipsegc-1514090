The included patch is to be applied to drupal core before importing terms or 
comments. This will allow terms and comments to come across with their tid/cid,
which should ease migration a bit.

It's advisable to revert the patch when migration is finished.
